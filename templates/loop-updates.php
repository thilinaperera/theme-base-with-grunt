<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/loop-updates.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<?php
$args = array(
    'post_type' => 'updates',
);
if(isset($GLOBALS['args'])){
    $args = array_merge($args,$GLOBALS['args']);
}
set_query_var( 'args', $args );
?>
<div class="row">
    <?php
    $the_query = new WP_Query($args);
    while ($the_query->have_posts()) {
        $the_query->the_post();
        get_template_part('templates/singles/single','update');
    }
    next_posts_link( 'Older Entries', $the_query->max_num_pages );
    previous_posts_link( 'Newer Entries' );
    wp_reset_postdata();
    ?>
</div>