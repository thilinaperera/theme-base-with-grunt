<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/MattressCareInstructions.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<?php get_template_part('templates/singles/component','ribbon-lg'); ?>
<h2 class="categoryHeading"><?php _e( 'Tips for your mattress care', 'woocommerce' ); ?></h2>
<?= get_field('tips_for_mattress_care') ?>
<h2 class="categoryHeading"><?php _e( 'Easy Steps for Caring for your non-flip mattress', 'woocommerce' ); ?></h2>
<?= get_field('tips_for_non_flip_mattress_care') ?>
<div class="col-lg-12">
    <p class="form-row closeHolder">
        <a href="" class="btn btn-submit closeBtn"><?php esc_attr_e( 'Close', 'woocommerce' ); ?></a>
    </p>
</div>


