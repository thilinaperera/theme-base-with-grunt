<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/popup-enquiry.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<?php get_template_part('templates/singles/component','ribbon-md'); ?>
<div class="col-lg-12">
    <form class="form-horizontal formEnquiry"  method="post">
        <div class="row messageContainer">
            <div class="messageHolder">
				<span class="icon">
					<i class="fa fa-check-circle"></i>
				</span>
				<span class="message">
					Congratulations! <br>You are now a proud owner of 8edtimes partner's products.
                </span>
            </div>
        </div>
        <div class="row inputHolderRow">
            <div class="col-lg-12">

                    <div class="form-group">
                        <label for="name_c" class="col-sm-3 control-label"><?php _e( 'Name', 'woocommerce' ); ?> <span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name_c" name="contact[name]"required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email_c" class="col-sm-3 control-label"><?php _e( 'Email Address', 'woocommerce' ); ?> <span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" id="email_c" name="contact[email]" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="no_c" class="col-sm-3 control-label"><?php _e( 'Contact No.', 'woocommerce' ); ?> <span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="no_c" name="contact[no]" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message_c" class="col-sm-3 control-label"><?php _e( 'Message', 'woocommerce' ); ?> :</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="message_c" name="contact[message]"></textarea>
                        </div>
                    </div>
            </div>
        </div>
        <p class="form-row closeHolder">
            <button type="submit" class="btn btn-submit submitButton" value=""><?php _e( 'Submit', 'woocommerce' ); ?></button>
            <a href="" class="btn btn-submit closeBtn"><?php esc_attr_e( 'Close', 'woocommerce' ); ?></a>
        </p>
    </form>
</div>
