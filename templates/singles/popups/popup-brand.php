<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/popup-mattressWarranty.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<?php get_template_part('templates/singles/component','ribbon'); ?>
<div class="col-lg-12">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 imageHolder text-center">
            <img src="<?= get_sub_field('brand_logo_lg')['url'] ?>" alt="">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 contentHolder">
            <h4>About This Brand</h4>
            <div class="content">
                <?= get_sub_field('brand_description') ?>
            </div>
        </div>
    </div>
    <p class="form-row closeHolder">
        <a href="/product-category/<?= get_sub_field('product_tag') ?>" class="btn btn-submit">view products</a>
        <a href="" class="btn btn-submit closeBtn"><?php esc_attr_e( 'Close', 'woocommerce' ); ?></a>
    </p>
</div>

