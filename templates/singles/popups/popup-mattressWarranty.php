<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/popup-mattressWarranty.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<?php get_template_part('templates/singles/component','ribbon-lg-o'); ?>
<h2 class="categoryHeading"><?php _e( "What is covered Under 8edtimes partner's 10 years limited warranty?", 'woocommerce' ); ?></h2>
<?= get_field('what_is_covered') ?>
<h2 class="categoryHeading"><?php _e("What is excluded under 8edtimes partner's warranty?", 'woocommerce' ); ?></h2>
<?= get_field('what_is_excluded') ?>
<div class="col-lg-12">
    <p class="form-row closeHolder">
        <a href="" class="btn btn-submit closeBtn"><?php esc_attr_e( 'Close', 'woocommerce' ); ?></a>
    </p>
</div>

