<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/ribbon.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<?php
$data = array(
    'heading' => get_field('ribbon_heading'),
    'content' => get_field('ribbon_content'),
    'top' => get_field('info_text_top'),
    'bottom' => get_field('info_text_bottom'),
);
if(isset($GLOBALS['data']['ribbon'])){
    if(isset($GLOBALS['data']['ribbon']) && $GLOBALS['data']['ribbon']['size'] == 'lg'){
        $data = array(
            'heading' => $GLOBALS['data']['ribbon']['title'],
            'content' => $GLOBALS['data']['ribbon']['content'],
            'top' => $GLOBALS['data']['ribbon']['top'],
            'bottom' => $GLOBALS['data']['ribbon']['bottom'],
        );
    }
}

?>
<div class="row ribbonHolder">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ribbonBox ribbonBoxLg">
        <div class="ribbonContainer">
            <img src="<?= IMAGES ?>/ribbon_border_lg.png" alt="">
            <div class="fullWidthContainer">
                <div class="table fullHeight">
                    <div class="title">
                        <h4><?= $data['heading'] ?></h4>
                        <div class="text">
                            <?= $data['content'] ?>
                        </div>
                    </div>
                    <div class="ribbon">
                        <div class="textHolder">
                            <div class="holder table fullHeight">
                                <div class="cell middle">
                                    <span class="topText"><?= $data['top'] ?></span>
                                    <span class="bottomText"><?= $data['bottom'] ?></span>
                                </div>
                            </div>
                        </div>
                        <img src="<?= IMAGES ?>/round_ribbon_lg.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
