<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/ribbon.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<?php
$class = '';
if(isset($GLOBALS['data'])){
    if(isset($GLOBALS['data']['ribbon']['position'])){
        $class = 'bottom';
    }
}

?>
<div class="row ribbonHolder <?= $class ?>">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ribbonBox">
        <div class="ribbonContainer">
            <img class="hidden-xs" src="<?= IMAGES ?>/ribbon_border.png" alt="">
            <img class="visible-xs" src="<?= IMAGES ?>/ribbon_border_lg.png" alt="">
            <div class="fullWidthContainer">
                <div class="table fullHeight">
                    <div class="title">
                        <?php if(isset($GLOBALS['data'])): ?>
                            <h3><?= $GLOBALS['data']['ribbon']['title'] ?></h3>
                        <?php else: ?>
                            <h3><?= get_field('banner_heading') ?></h3>
                        <?php endif; ?>
                    </div>
                    <div class="ribbon">
                        <div class="textHolder">
                            <div class="holder table fullHeight">
                                <div class="cell middle">
                                    <?php if(isset($GLOBALS['data'])): ?>
                                        <?php if( isset($GLOBALS['data']['id']) && $GLOBALS['data']['id'] == 'brand'){ ?>
                                            <span class="topText">Our</span>
                                            <span class="bottomText">Partner</span>
                                        <?php }else{ ?>
                                            <span class="topText"><?= $GLOBALS['data']['ribbon']['top'] ?></span>
                                            <span class="bottomText"><?= $GLOBALS['data']['ribbon']['bottom'] ?></span>
                                        <?php } ?>
                                    <?php else: ?>
                                        <span class="topText"><?= get_field('ribbon_text_top') ?></span>
                                        <span class="bottomText"><?= get_field('ribbon_text_bottom') ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <img src="<?= IMAGES ?>/round_ribbon.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $GLOBALS['data'] = array() ?>