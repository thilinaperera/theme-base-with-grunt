<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/ribbon.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 loginForm">
    <form class="form-horizontal formLogin"  method="post">

        <?php do_action( 'woocommerce_login_form_start' ); ?>

        <div class="form-group">
            <label for="username" class="col-sm-3 control-label"><?php _e( 'Email / Username', 'woocommerce' ); ?> <span class="required">*</span>:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="username" name="username" placeholder="" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" required>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span>:</label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="password" placeholder="" name="password" required>
            </div>
        </div>

        <?php do_action( 'woocommerce_login_form' ); ?>
        <div class="form-group">
            <div class="col-sm-12 submitHolder">
                <?php wp_nonce_field( 'woocommerce-login' ); ?>
                <input type="submit" class="btn btn-submit" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
                <?php
                if(isset( $GLOBALS['redirect'] )) :
                    ?>
                    <input type="hidden" name="redirect" value="<?php echo esc_url( $GLOBALS['redirect'] ) ?>" />
                    <?php
                endif;
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="rememberme" value="forever"> <?php _e( 'Remember me', 'woocommerce' ); ?>
                    </label>
                </div>
                <p class="lost_password">
                    <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
                </p>
                <?php if(isset($GLOBALS['type'])): ?>
                    <?php if($GLOBALS['type'] != 'corporate'): ?>
                        <p class="signupLink">
                            <?php _e( 'Not a member yet?', 'woocommerce' ); ?>  <a href="<?= get_permalink( get_page_by_title( 'Sign Up' )->ID ) ?>"><?php _e( "Sign up with us today, it's free.", 'woocommerce' ); ?></a>
                        </p>
                    <?php endif; ?>
                <?php else: ?>
                    <p class="signupLink">
                        <?php _e( 'Not a member yet?', 'woocommerce' ); ?>  <a href="<?= get_permalink( get_page_by_title( 'Sign Up' )->ID ) ?>"><?php _e( "Sign up with us today, it's free.", 'woocommerce' ); ?></a>
                    </p>
                <?php endif; ?>

            </div>
        </div>


        <?php do_action( 'woocommerce_login_form_end' ); ?>
    </form>
</div>
