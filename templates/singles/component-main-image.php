<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/component-main-image.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>

<div class="row mainImageHolder">
    <div class="col-lg-12 imageHolder">
        <img src="<?= get_field('main_image')['url'] ?>" alt="">
    </div>
</div>
