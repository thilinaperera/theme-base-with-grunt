<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/update.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<div class="col-lg-12 update">
    <h4><?= get_the_title() ?></h4>
    <span class="entry-date">Published on <?= get_the_date(); ?></span>
    <div class="contents">
        <?= the_post_thumbnail( 'large' ); ?>
        <div class="excerpt">
            <?= get_the_excerpt() ?>
        </div>
    </div>
    <div class="buttonHolder">
        <a href="<?= get_the_permalink() ?>" class="btn btn-normal">read more</a>
        <a popup="#enquiry" class="btn btn-normal link">enquiry</a>
        <a class="btn btn-normal" target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>"><i class="fa fa-facebook"></i> Share</a>
    </div>
</div>

