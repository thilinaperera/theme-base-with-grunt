<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/component-popup.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<div class="popupContent <?= $GLOBALS['data']['id'] ?>">
    <?php get_template_part('templates/singles/popups/popup',$GLOBALS['data']['id']); ?>
</div>
