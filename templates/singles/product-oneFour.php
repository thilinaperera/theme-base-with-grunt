<?php
/**
 * 1/4 Grid Product loop
 *
 * common product loop for each categories
 *
 * @link template/singles/product-1-4.php
 * @since 0.0.1
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<!-- product 1-4 template -->
<?php

            $item = new WC_product(get_the_id());
            $data = array(
                'image_shop' => $item->get_image( 'shop_thumbnail',  $attr = array()  ),
                'thumbnail' => $item->get_image( 'medium',  $attr = array() ),
                'price' => $item->get_price( ),
                'price_regular' => $item->get_regular_price( ),
                'price_sale' => $item->get_sale_price( ),
                'on_sale' => $item->is_on_sale( ),
                'discount' => ''
            );
            if($data['on_sale']){
                $data['discount'] = round(  ( ( $data['price_regular'] - $data['price_sale'] )*100 ) / $data['price_regular']  );
            }
            ?>


            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 productOneFour">
                <div class="thumbnail">
                    <a href="<?=  get_the_permalink()  ?>" class="imageHolder">
                        <?php if($data['on_sale']) : ?>
                        <div class="ribbon sales">
                            <img src="<?= IMAGES ?>/ribbon.png" alt="">
                            <div class="text">
                                <span><?= $data['discount'] ?>% off</span>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?= $data['thumbnail'] ?>
                    </a>
                    <div class="caption">
                        <h3><a href="<?=  get_the_permalink()  ?>"><?= get_the_title() ?></a></h3>

                        <p class="description"><?= wp_trim_words( get_the_content(), $num_words = 15, $more = ' ...' ); ?></p>

<!--                    <span class="price">--><?//= $data['price'] ?><!-- --><?//= get_woocommerce_currency(); ?><!--</span>-->
                        <span class="price"><?php echo $item->get_price_html(); ?></span>
                    </div>
                </div>
            </div>

