<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/ribbon.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<div class="row ribbonHolder">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ribbonBox ribbonBoxLg">
        <div class="ribbonContainer">
            <img src="<?= IMAGES ?>/ribbon_border_lg.png" alt="">
            <div class="fullWidthContainer">
                <div class="table fullHeight">
                    <div class="title">
                        <h4><?= get_field('ribbon_heading_o') ?></h4>
                        <div class="text">
                            <?= get_field('ribbon_content_o') ?>
                        </div>
                    </div>
                    <div class="ribbon">
                        <div class="textHolder">
                            <div class="holder table fullHeight">
                                <div class="cell middle">
                                    <span class="topText"><?= get_field('info_text_top_o') ?></span>
                                    <span class="bottomText"><?= get_field('info_text_bottom_o') ?></span>
                                </div>
                            </div>
                        </div>
                        <img src="<?= IMAGES ?>/round_ribbon_lg.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
