<?php
/**
 * Accessories
 *
 * Accessories product loop
 *
 * @link /templates/loop-mettress.php
 * @since 0.0.1
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */
?>
<h3 class="categoryHeading">Accessories</h3>
<?php
$args = array(
    'posts_per_page' => 4,
    'product_cat' => 'accessories',
    'post_type' => 'product',
    'meta_query' => array(
        array(
            'key' => '_stock_status',
            'value' => 'instock',
        )
    ),
    'meta_key' => 'total_sales',
    'orderby' => 'meta_value_num',
);
set_query_var( 'args', $args );
?>
    <div class="row">
        <?php
        $the_query = new WP_Query($args);
        $i = 1;
        while ($the_query->have_posts()) {
            $the_query->the_post();
                get_template_part('templates/singles/product','oneFour');
            if( $i%4 == 0 ):
                ?>
                <div class="clear"></div>
                <?php
            endif;
            $i++;
            }
        wp_reset_postdata();
        ?>
    </div>

