<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link ${DIRECTORY}/home.php
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */

?>
<?php if ( have_posts() ) : ?>
    <?php while( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <?php
    endwhile;
endif;
?>