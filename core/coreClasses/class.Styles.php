<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link URL
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */

namespace WpThemes\Core;

class Styles {
    public $dir;
    public $styles =array();
    public $special =array();
    public function __construct($styles = array(),$special = array()){
        $this->dir = STYLES;

        if(isset($styles)){
            foreach($styles as $name => $file){
                $name = strtolower(str_replace(' ', '_', $name));
                $filename = $file.'.css';
                array_push($this->styles,array(
                    'name' => $name,
                    'filename' => $filename
                ));
            }
            add_action('wp_enqueue_scripts', array($this,'init'));
        }
        if(isset($special)){
            foreach($special as $name => $file){
                $name = strtolower(str_replace(' ', '_', $name));
                $filename = $file.'.css';
                array_push($this->special,array(
                    'name' => $name,
                    'filename' => $filename,
                ));
            }
            add_action('wp_enqueue_scripts', array($this,'specialInit'));
        }

    }
    public function init(){

        foreach($this->styles as $single){
            wp_enqueue_style($single['name'], $this->dir . '/'.$single['filename'], array(),VERSION);
        }

    }
    public function specialInit(){

        foreach($this->special as $single){
            wp_enqueue_style($single['name'], THEMEROOT . '/'.$single['filename'], array(),VERSION);
        }

    }
}