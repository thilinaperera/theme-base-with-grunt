<?php
/**
 * Summary (no period for file headers)
 *
 * Description. (use period)
 *
 * @link URL
 * @since x.x.x (if available)
 *
 * @package WordPress
 * @subpackage 8edtimes
 * @author Thilina Perera
 */

namespace WpThemes\Core;

class Scripts {
    public $dir;
    public $scripts =array();
    public function __construct($scripts = array()){
        $this->dir = JAVASCRIPTS;

        if(isset($scripts)){
            foreach($scripts as $name => $file){
                $name = strtolower(str_replace(' ', '_', $name));
                $filename = $file.'.js';
                array_push($this->scripts,array(
                    'name' => $name,
                    'filename' => $filename
                ));
            }
            add_action('wp_enqueue_scripts', array($this,'init'));
        }

    }
    public function init(){

        foreach($this->scripts as $single){
            wp_enqueue_script($single['name'], $this->dir . '/'.$single['filename'], array(),VERSION);
        }

    }
}