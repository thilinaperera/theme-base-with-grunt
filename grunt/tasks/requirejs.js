// compile requirejs files for production
// http://requirejs.org
// https://github.com/gruntjs/grunt-contrib-requirejs
module.exports = function( grunt ){
    return {
        build: {
            options: {
                baseUrl: '<%= options.src %>/scripts',
                include: '../_lib/almond/almond', // assumes a production build using almond
                out: '<%= options.build %>scripts/rdt.js',
                name: 'main',
                mainConfigFile: '<%= options.src %>/scripts/sprout-zmain.js',
                optimize: "none"
            }
        },
        deploy: {
            options: {
                baseUrl: '<%= options.src %>/scripts',
                include: '../_lib/almond/almond', // assumes a production build using almond
                out: '<%= options.deploy %>/scripts/rdt.js',
                name: 'main',
                mainConfigFile: '<%= options.src %>/scripts/sprout-zmain.js'
            }
        }
    };
};