// Compile Sass files

module.exports = function(grunt) {
	return {
        options: {
        	quiet: false
        },
        build: {
        	options: {
        		style: 'expanded'
        	},
        	files: {
        		'style.css' : 'stylesheets/master.sass'
        	}
        },
        local: {
            options: {
                style: 'expanded'
            },
            files: {
                'style.css' : 'stylesheets/master.sass'
            }
        },
        deploy: {
        	options: {
        		style: 'compressed'
        	},
        	files: {
        		'style.css' : 'stylesheets/master.sass'
        	}
        }
	};
};