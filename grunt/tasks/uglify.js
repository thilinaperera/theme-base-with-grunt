// Minify production.js

module.exports = function(grunt) {
	return {
        lib : {
            files: {
                'js/jquery.plugins.min.js': 'js/plugins/*.js',
                'js/jquery.libs.min.js': 'js/lib/*.js',
            }
        },
        all : {
            files : {
                'js/production.min.js': ['js/build/development.js']
            }
        },
        deploy : {
            files : {
                'js/scripts.min.js': ['js/jquery.libs.min.js','js/jquery.plugins.min.js','js/build/development.js']
            }
        }
	};
};