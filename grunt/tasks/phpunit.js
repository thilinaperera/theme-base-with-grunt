/**
 * Created by thilina.perera on 9/30/2015.
 */
module.exports = function( grunt ){
    return {
        classes: {
            dir: '/'
        },
        options: {
            logTap: 'storage/logs/tests.log',
            colors: true
        }
    };
};