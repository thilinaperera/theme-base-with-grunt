// Compile Sass files

module.exports = function(grunt) {
	return {
        options: {
        	quiet: false
        },
        build: {
        	options: {
        		style: 'expanded'
        	},
        	files: {
        		'style.css' : 'stylesheets/build/master.sass'
        	}
        },
        deploy: {
        	options: {
        		style: 'compressed'
        	},
        	files: {
        		'style.css' : 'stylesheets/build/master.sass'
        	}
        }
	};
};