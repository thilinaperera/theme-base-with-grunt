// Watch files for changes

module.exports = function(grunt) {
	return {
		dist: {
            src: [
                'js/dev/*.js', //All JS in the libs folder
            ],
            dest: 'js/build/development.js',
        }
	};
};