// Replace call to non-minified js in deploy

module.exports = function(grunt) {
	return {
        deploy: {
			src: ['<%= options.deploy %>/**/*.php'],
			overwrite: true,
			replacements: [
				{
					from: '#VERSION',
					to: '<%= pkg.cache %>'
				}
			]
		},
        local: {
		    src: ['<%= options.local %>/**/*.php'],
		    overwrite: true,
            replacements: [
                {
                    from: '#VERSION',
                    to: '<%= pkg.cache %>'
                }
            ]
		}
	};
};
