// Allows us to add a version number to style.css

module.exports = function(grunt) {
	return {
		version: {
		    defaults: {
			    src: ['style.css', 'js/deploy/production.min.js']
		    }
		}
	}
}