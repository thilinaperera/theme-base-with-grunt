// compile sass files for production
// https://github.com/gruntjs/grunt-contrib-sass
module.exports = function( grunt ){
    return {
        build: ["<%= options.build  %>/*", "!<%= options.build  %>/README"],
        deploy: ["<%= options.deploy  %>/*", "!<%= options.deploy  %>/README"]    
    };
};
