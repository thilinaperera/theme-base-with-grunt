// Generate static site
// https://github.com/assemble/assemble
module.exports = function( grunt ){

    return {
        options: {
            /*postprocess: require('pretty'),*/
            marked: {sanitize: false},
            data: '<%= options.src %>/**/*.json',
            helpers: '<%= options.src %>/helpers/helper-*.js',
            layoutdir: '<%= options.src %>/templates',
            partials: ['<%= options.src %>/patterns/**/*.hbs']
        },
        build: {
            options: {
                layout: 'base.hbs',
                assets: '<%= options.build %>',
            },
            files: [
                {
                    expand: true,
                    cwd: '<%= options.src %>', 
                    src: ['**/*.hbs','!**/templates/**'], 
                    dest: '<%= options.build %>'
                }
			]
		},
       
        deploy: {
            options: {
                layout: 'base.hbs',
                assets: '<%= options.deploy %>'            },
            files: [
                {
                    expand: true,
                    cwd: '<%= options.src %>', 
                    src: ['**/*.hbs','!**/templates/**','*.php'], 
                    dest: '<%= options.deploy %>'
                }
			]
        },
    }
};
