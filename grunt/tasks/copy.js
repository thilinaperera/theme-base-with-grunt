// Copy the files to the build/deploy directories.

module.exports = function(grunt) {
	return {
        build: {
        	files: [
                {
                    expand: true,
                    src: [
                        'core/**'
                    ],
                    dest: '<%= options.build %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'woocommerce/**'
                    ],
                    dest: '<%= options.build %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'templates/**'
                    ],
                    dest: '<%= options.build %>',
                    filter: 'isFile'
                },
        		{
        			expand: true,
        			src: [
        				'images/**'
        			],
        			dest: '<%= options.build %>',
                    filter: 'isFile'
        		},
                {
                    expand: true,
                    src: [
                        'styles/**'
                    ],
                    dest: '<%= options.build %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'fonts/**'
                    ],
                    dest: '<%= options.build %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'js/*.*',
                        'js/build/*',
                        'js/other/*'
                    ],
                    dest: '<%= options.build %>'
                },
                {
                    expand: true,
                    src: [
                        '*.php',
                        '*.css',
                        '*.map',
                        '*.txt',
                        'screenshot.png',
                        'includes/**',
                        'acf/**'//,
                        //'immersive/**'
                        //'!includes/build-head.php'
                    ],
                    dest: '<%= options.build %>'
                }
        	]    
        },
        local: {
        	files: [
                {
                    expand: true,
                    src: [
                        'core/**'
                    ],
                    dest: '<%= options.local %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'woocommerce/**'
                    ],
                    dest: '<%= options.local %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'templates/**'
                    ],
                    dest: '<%= options.local %>',
                    filter: 'isFile'
                },
        		{
        			expand: true,
        			src: [
        				'images/**'
        			],
        			dest: '<%= options.local %>',
                    filter: 'isFile'
        		},
                {
                    expand: true,
                    src: [
                        'styles/**'
                    ],
                    dest: '<%= options.local %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'fonts/**'
                    ],
                    dest: '<%= options.local %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'js/*.*',
                        'js/build/*',
                        'js/other/*'
                    ],
                    dest: '<%= options.local %>'
                },
                {
                    expand: true,
                    src: [
                        '*.php',
                        '*.css',
                        '*.map',
                        '*.txt',
                        'screenshot.png',
                        'includes/**',
                        'acf/**'//,
                        //'immersive/**'
                        //'!includes/build-head.php'
                    ],
                    dest: '<%= options.local %>'
                }
        	]
        },
        deploy: {
        	files: [
                {
                    expand: true,
                    src: [
                        'core/**'
                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'woocommerce/**'
                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'templates/**'
                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'images/**'
                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'styles/**'
                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'fonts/**'
                    ],
                    dest: '<%= options.deploy %>',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    src: [
                        'js/*.*',
                        'js/build/*',
                        'js/other/*'
                    ],
                    dest: '<%= options.deploy %>'
                },
                {
                    expand: true,
                    src: [
                        '*.php',
                        '*.css',
                        '*.map',
                        '*.txt',
                        'screenshot.png',
                        'includes/**',
                        'immersive/**',
                        'acf/**'
                    ],
                    dest: '<%= options.deploy %>'
                }
            ]
        },
        css:{
            files: [
                {
                    expand: true,
                    src: [
                        '*.css'
                    ],
                    dest: '<%= options.local %>'
                }
            ]
        }
	};
};