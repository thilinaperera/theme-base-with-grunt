// Watch files for changes

module.exports = function(grunt) {
	return {
		scripts: {
            files: ['js/dev/**/*.js'],
            tasks: ['concat', 'uglify'],
            options: {
                spawn: false
            }
        },
        css: {
            files: ['stylesheets/**/*.scss','stylesheets/**/*.sass'],
            tasks: ['sass:build','copy:css'],
            options: {
                debounceDelay: 250
            }
        }
	};
};