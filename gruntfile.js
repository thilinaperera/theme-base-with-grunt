module.exports = function(grunt) {

    var ifOptionElse = function(option, alternate) {
        return grunt.option(option) || alternate;
    };

    var config = {
        pkg: grunt.file.readJSON('package.json'),

        options: {
            src: 'src',
            build: ifOptionElse('build-path', 'build/8edtimes-wp'),
            local: ifOptionElse('build-path', '<%= pkg.theme_local_path %>/8edtimes-wp'),
            deploy: ifOptionElse('deploy-path', 'deploy/8edtimes-wp'),
            staticUrl: ifOptionElse('static-url', '')
        },
        clean: {
            build: ['<%= options.deploy %>']
        },
        wordpressdeploy: {
            options: {
                backup_dir: "backups/",
                rsync_args: ['--verbose', '--progress', '-rlpt', '--compress', '--omit-dir-times', '--delete'],
                exclusions: ['Gruntfile.js', '.git/', 'tmp/*', 'backups/', 'wp-config.php', 'composer.json', 'composer.lock', 'README.md', '.gitignore', 'package.json', 'node_modules']
            },
            local: {
                "title": "local",
                "database": "mktit_merlin_db",
                "user": "root",
                "pass": "",
                "host": "localhost",
                "url": "http://localhost/hp-merlin/trunk/src/sites/merlin",
                "path": "/hp-merlin/trunk/src/sites/sprout/wp"
            },
            dev: {
                "title": "dev",
                "database": "mktit_merlin_db",
                "user": "mktit_merlin_db",
                "pass": "Welcome-1234",
                "host": "g1t4739.austin.hp.com:1531",
                "url": "http://c1t02099.itcs.hp.com/merlin/sites/merlin",
                "path": "/merlin/sites/merlin",
                "ssh_host": "mktit_merlin_db@g1t4739.austin.hp.com"
            }
        }
    };
    
    //1. All configuration goes here

    'concat copy jshint sass uglify watch replace version phpunit'.split(' ').forEach(function(task){
        config[task] = require('./grunt/tasks/'+task)(grunt, config.options);
    });
    grunt.initConfig(config);

    //

    //2. Where we tell Grunt which plugins we plan on using (as defined in package.json)

    Object.keys(require('./package.json').devDependencies).forEach(function(dep){
        if(dep.match(/grunt-/) || dep.match('assemble')) {
            grunt.loadNpmTasks(dep);
        }
    });

    //

    //3 Where we tell Grunt what to do when we type 'grunt' into the terminal

    grunt.registerTask('default', ['concat', 'sass:build', 'uglify:lib', 'uglify:all', 'watch']);
    grunt.registerTask('local', ['concat', 'sass:local','uglify:lib','uglify:all','copy:local', 'replace:local']);
    grunt.registerTask('deploySubTask', ['concat','sass:deploy', 'uglify:lib', 'uglify:all','copy:deploy', 'replace:deploy']);
    grunt.registerTask('test', ['phpunit']);
    grunt.registerTask('css', ['watch']);

    //

    //4 Configure deploySubTask

    grunt.registerTask('deploy', function() {
        var opt = function(name){
            return grunt.option(name) ? name : null;
        };
        var target = opt('production') || opt('development') || opt('itg');
        if( !target ){
            grunt.log.writeln('You must specify a target, either --development, --production, or --itg');
            return;
        }
        grunt.task.run(['deploySubTask']);
    });

};