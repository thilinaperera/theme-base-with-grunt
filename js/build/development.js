/**
 * Created by thilina.perera on 9/30/2015.
 */
var urlBuilder =  function(){
    var path = window.location.pathname;
    var host = window.location.origin;
    var pathObj = _.compact( path.split('/') );
    return host+'/'+pathObj[0]+'/'+'wp-content/themes/8edtimes-wp/ajax.php';

};
var components = {
    mapsObj : [],
    init : function () {
        this.mobileMenu();
        this.mainSlider();
        this.placeHolder([ jQuery('.signUpFormHolder label') , jQuery('.e-warrantyForm label')]);
        this.removeAutoFill();
        /**
         * Validations
         */
        this.validation($('.registerForm'));
        this.validation($('.e-warrantyForm'));
        this.validation($('.formContact'));
        this.validation($('.formLogin'));

        this.sorting($('.tree'));
        this.priceFilter();
        this.reponsiveImage($('figure.responsive'));
        this.popups();

        this.showHideForms();
        this.fitText($('.ribbonBox').find('.title'));
    },
    fitText : function (elem) {
        //elem.fitText();
    },
    mobileMenu : function(){
        jQuery('.mobileMenu').slicknav({
            duplicate: false,
            prependTo : '.mobileMenuHolder'
        });
    },
    mainSlider : function () {
        jQuery('#slider').nivoSlider({
            effect: 'fade',                 // Specify sets like: 'fold,fade,sliceDown'
            slices: 15,                     // For slice animations
            boxCols: 8,                     // For box animations
            boxRows: 4,                     // For box animations
            animSpeed: 500,                 // Slide transition speed
            pauseTime: 5000,                 // How long each slide will show
            startSlide: 0,                     // Set starting Slide (0 index)
            directionNav: false,             // Next & Prev navigation
            controlNav: true,                 // 1,2,3... navigation
            controlNavThumbs: false,         // Use thumbnails for Control Nav
            pauseOnHover: false,             // Stop animation while hovering
            manualAdvance: false,             // Force manual transitions
            prevText: 'Prev',                 // Prev directionNav text
            nextText: 'Next',                 // Next directionNav text
            randomStart: false,             // Start on a random slide
            beforeChange: function(){},     // Triggers before a slide transition
            afterChange: function(){},         // Triggers after a slide transition
            slideshowEnd: function(){},     // Triggers after all slides have been shown
            lastSlide: function(){},         // Triggers when last slide is shown
            afterLoad: function(){}         // Triggers when slider has loaded
        });
    },
    placeHolder : function (elems) {
        jQuery.each(elems,function (k,v) {
            v.inFieldLabels();
        })
    },
    removeAutoFill : function () {
        $('input').each(function () {
            if($(this).attr('name') == 'password'){
                var elem = $(this);
                setTimeout(function(){
                    //elem.focus().trigger('click')
                },300)

            }
        })
    },
    validation : function (form){
        var $this = this;
        if(form.hasClass('registerForm')){
            form.validate({
                rules: {
                    password:{
                        required: true,
                        minlength: 8
                    },
                    verify_password: {
                        required: true,
                        equalTo: "#reg_password"
                    }
                },
                messages : {
                    verify_password : 'Please enter same password again'
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        }
        else if(form.hasClass('formEnquiry')){
            form.validate({
                submitHandler: function(form) {
                    $(form).ajaxSubmit($this.ajaxOptions.popup);
                    return false;
                }
            })
        }
        else if(form.hasClass('e-warrantyForm')){
            form.validate({
                submitHandler: function(form) {
                    $(form).ajaxSubmit($this.ajaxOptions.ewarranty);
                    return false;
                }
            })
        }
        else if(form.hasClass('formContact')){
            form.validate({
                submitHandler: function(form) {
                    $(form).ajaxSubmit($this.ajaxOptions.contact);
                    return false;
                }
            })
        }
        else{
            form.validate({
                submitHandler: function(form) {
                    form.submit();
                }
            })
        }

    },
    sorting : function (elem) {
        var trigger = elem.children('.treeHeading');
        var vars = elem.children('.treeHolder');

        var form = $(elem).parent('.woocommerce-ordering');
        var dataInput = $( '<input class="sortval" type="hidden" name="orderby"/>');
        form.append(dataInput);
        vars.children('li').children('a').on('click',function(e){
            e.preventDefault();
            var orderBy = $(this).attr('data');
            $(dataInput).attr('value',orderBy);
            form.submit();
        })
    },
    priceFilter : function(){
        $( ".price_slider" ).on( "slidestop", function( event, ui ) {

            var form = $( this ).parent().parent()
            if(form.is('form')){
                form.submit();
            }

        } );
    },
    reponsiveImage : function(elem){
        //elem.picture();
    },
    popups : function(){
        var $this = this;
        jQuery('a.link').featherlight({
            targetAttr: 'popup',
            afterOpen : function(){

                jQuery('body').css({
                    overflow : 'hidden'
                });
                $('.btn.closeBtn')
                    .unbind()
                    .on('click', function (e) {
                        e.preventDefault();
                        var current = $.featherlight.current();
                        current.close();
                    });

                /**
                 * From validation
                 */
                $('.featherlight').find('.formEnquiry').each(function () {
                    if(!$(this).attr('novalidate')){
                        $this.validation($(this));
                    }
                })
            },
            afterClose: function(){
                jQuery('body').css({
                    overflow : ''
                })
            }
        });
    },
    maps : function (elem) {
        var $this = this;
        if($(elem).length > 0 ){
            $(elem).each(function () {
                var e = $(this);
                var id = e.attr('id');
                var lat = e.attr('lat');
                var lng = e.attr('lng');

                var map = new GMaps({
                    el: '#'+id,
                    lat: lat,
                    lng: lng
                });
                map.addMarker({
                    lat: lat,
                    lng: lng,
                    title: 'Lima'
                });
                $(window).resize(function() {
                    google.maps.event.trigger(map, 'resize');
                });
                google.maps.event.trigger(map, 'resize');
                $this.mapsObj.push(map);
            })
        }

    },
    showHideForms : function () {
        $('.normalLogin').click(function (e) {
            e.preventDefault();
            $('.corporateForm').hide();
            $('.normalForm').show();
        });
        $('.corporateLogin').click(function (e) {
            e.preventDefault();
            $('.corporateForm').show();
            $('.normalForm').hide();
        })
    },
    ajaxOptions: {
        popup : {
            url : urlBuilder(),
            success : function (responseText, statusText, xhr, $form) {
                if(responseText.code == '200'){
                    $form.children('.inputHolderRow').hide();
                    $form.find('.submitButton').hide();
                    $form.children('.messageContainer')
                        .show()
                        .find('.message').html(responseText.message)
                }
                console.log(responseText);

            },
            beforeSubmit : function (arr, $form, options) {

                $form.find('.submitButton')
                    .attr('disabled','disabled')
                    .html('<img src="/wp-admin/images/spinner.gif">')
                    .css({
                        'background' : 'rgba(124,153,168,.3)'
                    })
            },
            error : function(e){
                console.log(e);
            }
        },
        contact : {
            url : urlBuilder(),
            success : function (responseText, statusText, xhr, $form) {
                if(responseText.code == '200'){
                    $form.children('.form-group').hide();
                    $form.children('.messageContainer')
                        .show()
                        .find('.message').html(responseText.message)
                }
                console.log(responseText);

            },
            beforeSubmit : function (arr, $form, options) {

                $form.find('.submitButton')
                    .attr('disabled','disabled')
                    .html('<img src="/wp-admin/images/spinner.gif">')
                    .css({
                        'background' : 'rgba(124,153,168,.3)'
                    })
            },
            error : function(e){
                console.log(e);
            }
        },
        ewarranty : {
            url : urlBuilder(),
            success : function (responseText, statusText, xhr, $form) {
                if(responseText.code == '302'){
                    var form = $("<form>", {
                        name: "formConfirmation",
                        id: "formConfirmationSubmission",
                        style: "display:none;",
                        action: "/e-warranty/e-confirmation/",
                        method: "post",
                        "accept-charset": 'utf-8',
                        target : ''
                    });



                    var data = responseText.data;
                    _.each(data, function(v, k, l){
                        var input = $("<input>", {
                            type: 'hidden',
                            name: 'confirm['+k+']',
                            value: v
                        });
                        form.append(input);
                    });

                    form.appendTo('body').submit();

                }
                console.log(responseText);

            },
            beforeSubmit : function (arr, $form, options) {

                $form.find('.submitButton')
                    .attr('disabled','disabled')
                    .html('<img src="/wp-admin/images/spinner.gif">')
                    .css({
                        'background' : 'rgba(124,153,168,.3)'
                    })
            },
            error : function(e){
                console.log(e);
            }
        }
    }

};

jQuery(document).ready(function () {
    window.jQuery = window.$ = jQuery;
    components.init();
});